#include <stdio.h>
#include <string.h>
#include "structDef.c"



int main() {

    Calculator calculatorMain = {
            100,
            "837B",
            "Sharp",
            "White",
            1
    };
    printf("----------------PART 1 --------------------------\n");

    printf("Price : %i \n Model : %s \n Brand : %s \n Color : %s \n Number of batteries : %i \n " ,
           calculatorMain.price, calculatorMain.model, calculatorMain.brand, calculatorMain.color, calculatorMain.numberOfBatteries);

    printf("------------------PART 2------------------------\n");


    calculatorMain = inicializarStruct(calculatorMain,250, "F5SX5", "Casio", "Black", 2);


    printf("Price : %i \n Model : %s \n Brand : %s \n Color : %s \n Number of batteries : %i \n " ,
           calculatorMain.price, calculatorMain.model, calculatorMain.brand, calculatorMain.color, calculatorMain.numberOfBatteries);

    printf("----------------PART 3 --------------------------\n");

    Child child1 = {
            11,
            "Marcos"
    };
    Child child2 = {
            23,
            "Nicolas"
    };

    Person person1;
    person1.age= 53;
    person1.name = "Gerardo";
    person1.child[0] = child1;
    person1.child[1] = child2;

    printf("%s  %i  \n Children : \n %s %d \n %s %d", person1.name, person1.age,
           person1.child[0].name, person1.child[0].age,
           person1.child[1].name, person1.child[1].age);

}