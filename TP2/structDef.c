#include <stdio.h>
#include <string.h>

typedef struct{

    int price;
    char *model;
    char *brand;
    char *color;
    int numberOfBatteries;

}Calculator;

Calculator inicializarStruct(Calculator  c,int price, char model[50], char brand[50], char color[50], int batteries) {

    c.price = price;
    c.model = model;
    c.brand = brand;
    c.color = color;
    c.numberOfBatteries = batteries;
    return c;

};

typedef struct {

    int age;
    char *name ;

}Child;

typedef struct {

    int age;
    char *name;
    Child child[2];

}Person;
